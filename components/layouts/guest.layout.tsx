/*
 * Project : simple-escort
 * Version : 0.1.0
 * File Created : Sunday, 18th July 2021 4:57:28 pm
 * Author : Pran Pegu (pranpegu997@gmail.com)
 * -----
 * Last Modified: Sunday, 18th July 2021 4:57:28 pm
 * Modified By: Pran Pegu (pranpegu997@gmail.com>)
 */
import { faArrowAltCircleRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Fragment } from "react";
import Navbar from "./navbar";

const GuestLayout = ({ children }: any) => {
    return (
        <Fragment>
            <div className="background">
                <div className="background_overlay">
                    <div className="background_content">
                        <Navbar />
                        <div className="home col-12 row justify-content-center">
                            <div className="home_content col-8">
                                <h2 className="home_title">
                                    You will Never Regret
                                </h2>
                                <h1 className="home_subtitle">
                                    <span>enjoy</span> every moment you live
                                </h1>
                                <div className="call_button">
                                    <h3>
                                        CALL NOW{" "}
                                        <FontAwesomeIcon
                                            icon={faArrowAltCircleRight}
                                        />
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {children}
        </Fragment>
    );
};

export default GuestLayout;
