/*
 * Project : simple-escort
 * Version : 0.1.0
 * File Created : Sunday, 18th July 2021 4:46:37 pm
 * Author : Pran Pegu (pranpegu997@gmail.com)
 * -----
 * Last Modified: Sunday, 18th July 2021 4:46:37 pm
 * Modified By: Pran Pegu (pranpegu997@gmail.com>)
 */

export { default as GuestLayout } from "./guest.layout";
