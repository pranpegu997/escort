/*
 * Project : simple-escort
 * Version : 0.1.0
 * File Created : Sunday, 18th July 2021 6:20:51 pm
 * Author : Pran Pegu (pranpegu997@gmail.com)
 * -----
 * Last Modified: Sunday, 18th July 2021 6:20:52 pm
 * Modified By: Pran Pegu (pranpegu997@gmail.com>)
 */

import * as faker from "faker";

const getGirl = () => {
    return {
        title: faker.random.arrayElement([
            "Sunny Baby",
            "Puja Baby",
            "Kajal Baby",
            "Kumari Baby",
            "Lovely Babe",
            "Pretty Babe",
        ]),
        image: faker.random.arrayElement([
            "/assets/images/girl1.jpg",
            "/assets/images/girl2.jpg",
            "/assets/images/girl3.jpg",
            "/assets/images/banner1.jpg",
            "/assets/images/banner2.jpg",
            "/assets/images/banner3.jpg",
            "/assets/images/banenr4.jpg",
        ]),
        location: faker.random.arrayElement([
            "Bangalore",
            "Patna",
            "Mumbai",
            "Delhi",
            "Karnataka",
            "Ludhiyana",
        ]),
        bio: "I love to disperse a healthy adventures for my clients.I'm inherently sized 34C, with tender and soft skin for one to touch.",
    };
};

export const GIRLS = (count: number = 1) => {
    const girls = [];

    for (let i = 0; i < count; i++) {
        girls.push(getGirl());
    }

    return girls;
};
